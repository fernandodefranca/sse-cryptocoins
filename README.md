# SSE-Cryptocurrencies (POC)

This is a proof of concept of a Express server application implementing [Server Sent Events](https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events).

The application fetches the top 10 cryptocurrencies values in BRL currency and populates a route with the last 50 results.

Subsequent updates are pushed to clients with Server Sent Events on a specific route.