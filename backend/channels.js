const SseChannel = require('sse-channel');

let crypto = new SseChannel({
  cors: { origins: ['*'] },
  retryTimeout: 250,
  pingInterval: 60 * 1000,
  jsonEncode: true,
});

module.exports = {
  crypto,
}