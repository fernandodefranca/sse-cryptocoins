"use strict"

const fetch = require('node-fetch')

let maxResults = 50
let lastResult = null
let bufferedResults = []

function parseResults(body) {
  let obj = JSON.parse(body)
  let newObj = {}
  
  Object.keys(obj).map((key) => {
    newObj[key] = obj[key].BRL
  });

  return newObj;
}

function startLoop(endpoint, loopDelay, resultCallback) {
  const loop = () => {
    fetch(endpoint)
    .then(res => res.text())
    .then((body) => {
      // Compara resultado com o anterior
      if (body!==lastResult){
        lastResult = parseResults(body)
        lastResult.timestamp = Date.now()
        
        bufferedResults.push(lastResult)
        
        // Mantem os ultimos "maxResults"
        if (bufferedResults.length>maxResults) bufferedResults.shift()

        resultCallback(lastResult)
      } else {
        resultCallback('no updates...')
      }
      setTimeout(loop, loopDelay)
    })
  }

  loop()
}

module.exports = {
  startLoop,
  bufferedResults,
  lastResult
}