const config = require('./config.json')

const express = require('express')
const path = require('path');

const port = config.server.port;
const app = express();

const fetchCryptoService = require('./fetchCryptoService');
const channels = require('./channels');

// https://api.coinmarketcap.com/v1/ticker/?convert=BRL&limit=10

fetchCryptoService.startLoop(config.endpoint, config.delay, (results) => {
  //console.log(results);
  results.connectionCount = channels.crypto.getConnectionCount()
  channels.crypto.send(JSON.stringify(results));
});

app.disable('x-powered-by');

app.use(express.static(path.join(__dirname, '../public')));

// CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/channel/crypto', (req, res) => {
  channels.crypto.addClient(req, res);
});

app.get('/latest/crypto', (req, res) => {
  res.send(fetchCryptoService.bufferedResults)
});

app.listen(config.server.port, config.server.address, () => {
  console.log(`Listening on http://${config.server.address}:${config.server.port}/`);
});