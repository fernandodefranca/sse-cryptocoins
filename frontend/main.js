import App from './App.html';
import { Store } from 'svelte/store.js';
import currencies from './currencies.json'
import crypto from './services/crypto'

class MyStore extends Store {
  constructor(defaults){
    super(defaults)

    crypto.getSeries()
    .then((arr) => {
      this.setCurrencySeries(arr)
    })

    crypto.subscribe((data) => {
      this.updateCurrencySeries(data)
    })
  }
  toggleCurrencySymbol(symbol){
    let updated = this.get('currencies').map((currency) => {
      if (currency.symbol!==symbol) return currency
      currency.active = !currency.active
      return currency
    })
    this.set({currencies: updated})
  }
  setConnectedClients(num){
    this.set({connectedClients:num})
  }
  setCurrencySeries(arr){
    this.set({currencySeries:arr})
  }
  updateCurrencySeries(newObj){
    let series = this.get('currencySeries')
    if (!series) return
    series.push(newObj)
    if (series.length > 600) series.shift()
    this.setCurrencySeries(series)
    this.set({connectedClients: newObj.connectionCount})
  }
}

const store = new MyStore({
  connectedClients: 1,
  currencies: currencies.map(currency => {
    currency.active = true
    return currency
  }),
  currencySeries: [],
})

const app = new App({
	target: document.body,
  store
});

window.store = store;
window.app = app;

export default app;