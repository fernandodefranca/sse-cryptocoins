const serverPath = `http://${document.location.hostname}:3333`

export default class crypto {

  // Obtem ultimos resultados do buffer
  static getSeries(){
    return fetch(`${serverPath}/latest/crypto`)
    .then(res => res.json())
    .catch(err => console.log(err))
  }

  static subscribe(fn){
    var es = new EventSource(`${serverPath}/channel/crypto`);
    es.onmessage = (e) =>{
      try{
        const parsed = JSON.parse(JSON.parse(e.data));
        fn(parsed)
      } catch(err){ console.log(err) }
    }
  }
}